import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.js";
import 'aos/dist/aos.css'
import { createPinia } from "pinia";



createApp(App)
.use(createPinia)
.use(router)
.mount("#app");

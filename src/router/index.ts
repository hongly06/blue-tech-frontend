import axios from "axios";
import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import HomeView from "../views/HomeView.vue";
import Product from "../views/Product.vue";
import Service from "../views/Service.vue";
import SubProduct from "../views/SubProduct.vue"
import Login from "../views/Login.vue";
import Register from "../views/Register.vue"
import Contact from "../views/Contact.vue"
import Industry from "../views/Industry.vue";
import IndustryDetail from '../views/IndustryDetail.vue'
import SubProductDetail from "../views/SubProductDetail.vue"
import UserProfile from "../views/Profile/UserProfile.vue"
// import ResetPassword from "../views/ResetPassword.vue"



const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
  {
    path: "/product",
    name: "product",
    component: Product,
  },

  {
    path: "/product/:id",
    name: "subProduct",
    component: SubProduct,
  },

  {
    path: "/product/sub-product-detail/:id",
    name: "subProductDetail",
    component: SubProductDetail,
  },

  {
    path: "/service",
    name: "service",
    component: Service,
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/register",
    name: "register",
    component: Register,
  },
  {
    path: "/contact",
    name: "contact",
    component: Contact,
  },
  {
    path: "/industry",
    name: "industry",
    component: Industry,
  },

  {
    path: "/industry/:id",
    name: "industryDetail",
    component: IndustryDetail,
  },
  // {
  //   path: "/reset-password",
  //   name: "ResetPassword",
  //   component: ResetPassword,
  // },

  {
    path: "/user",
    name: "user",
    // before login check route first if no authentication than redirect to login page
    beforeEnter: guardRoute,
    component: UserProfile,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

// grobal api
// axios.defaults.baseURL ='http://localhost:4000/'
axios.defaults.baseURL ='https://seahorse-app-ih22h.ondigitalocean.app/blue-tech-api2/'


// guard route if not authenticated
function guardRoute(to: any , from: any , next: any){
  let isAuthenticated = false
  if(localStorage.getItem('token')){
    isAuthenticated = true
  }else{
    isAuthenticated = false
  }
  if(isAuthenticated){
    next()
  }else{
    next('/login')
  }

}


export default router;

export interface IData {
  total?: number;
  items?: IService[];
}

 interface IService {
  _id?: string;
  title?: string;
  description?: string;
  image?: string;
}
